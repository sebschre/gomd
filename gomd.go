package main

import (
	"fmt"

	"gitlab.com/sebschre/gomd/neighborlist"
)

func main() {
	s := []int{7, 2, 8, -9, 4, 0}

	c := make(chan int)
	go neighborlist.Sum(s[:len(s)/2], c)
	go neighborlist.Sum(s[len(s)/2:], c)
	x, y := <-c, <-c // receive from c

	fmt.Println(x, y, x+y)

	i := 42
	var p = &i
	fmt.Println(*p)
}
