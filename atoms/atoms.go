// Package atoms ...
package atoms

import "gonum.org/v1/gonum/mat"

// Cell defintion
type Cell struct {
	latticeVec1, latticeVec2, latticeVec3 [3]float64
}

// Position ...
type Position struct {
	X, Y, Z [3]float64
}

// Atom ...
type Atom struct {
	Cell
	Position
}

// Atoms ...
type Atoms struct {
	N         int
	atomSlice []Atom
}

func matmult(a Atoms) mat.Dense {
	var b mat.Dense
	c := b
	return c
}
